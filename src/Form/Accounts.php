<?php
namespace Drupal\nostr\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * Provides a configuration form for managing items.
 */
class Accounts extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nostr.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nostr_accounts';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the configuration settings.
    $config = $this->config('nostr.settings');

    // Add button to add items.
    $form['add_button'] = [
      '#type' => 'button',
      '#value' => $this->t('Add'),
      '#ajax' => [
        'callback' => '::addButtonAjaxCallback',
        'wrapper' => 'items-list-wrapper',
      ],
    ];

    // Display added items in a list.
    $form['accounts'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'items-list-wrapper'],
    ];

    $items = $config->get('accounts') ?? [];
    foreach ($items as $key => $item) {
      $form['accounts'][$key] = [
        '#markup' => $item,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * AJAX callback for the "Add" button.
   */
  public function addButtonAjaxCallback(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('nostr.settings');

    // Add a new item to the configuration.
    $items = $config->get('accounts') ?? [];
    $items[] = 'New Account'; // You can customize this value based on your requirements.
    $config->set('accounts', $items)->save();

    // Rebuild the form to update the displayed items.
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#items-list-wrapper', $form['accounts']));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration settings.
    parent::submitForm($form, $form_state);
  }
}
