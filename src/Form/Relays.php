<?php

namespace Drupal\nostr\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Configure Nostr Core settings for this site.
 */
class Relays extends ConfigFormBase {

  use MessengerTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nostr_relays';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nostr.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['relay'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Relay'),
      '#default_value' => $this->config('nostr.settings')->get('relay'),
    ];
    if ($relay = substr($this->config('nostr.settings')->get('relay'), 6)) {
      $url = 'https://' . $relay;
      $options = [
        'headers' => [
          'Accept'     => 'application/nostr+json',
        ]
      ];
      try {
        $relay_info = Json::decode(\Drupal::httpClient()->request('GET', $url, $options)->getBody());
      }
      catch (\Exception $e) {
        // Handle the exception.
        \Drupal::logger('nostr')->error('An error occurred: @message', ['@message' => $e->getMessage()]);
        $error = t('The Relay @relay is not responding.', ['@relay' => $url]);
        $this->messenger()->addError($error);
      }
      if (isset($relay_info) && is_array($relay_info)){
        $relay_name = $relay_info['name'];
        $relay_description = $relay_info['description'];
        $relay_software = $relay_info['software'];
        $relay_supported_nips = implode(', ', $relay_info['supported_nips']);
        $relay_info_string = "<p>
          <b>Name:</b> $relay_name<br />
          <b>Description:</b> $relay_description<br />
          <b>Software:</b> $relay_software<br />
          <b>Supported NIPs:</b> $relay_supported_nips<br />
  </p>";
        $form['relay_info'] = [
          '#type' => 'item',
          '#title' => $this->t('Relay Information'),
          '#markup' => $relay_info_string,
        ];
      }

    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (substr($form_state->getValue('relay'), 0, 6) != 'wss://') {
      $form_state->setErrorByName('relay', $this->t('The relay address must begin with wss://.'));
    }
    else {
      $url = 'https://' . substr($form_state->getValue('relay'), 6);
      $options = [
        'headers' => [
          'Accept'     => 'application/nostr+json',
        ]
      ];
      $result = Json::decode(\Drupal::httpClient()->request('GET', $url, $options)->getBody());
      if (!isset($result['name'])) {
        $form_state->setErrorByName('relay', $this->t('This is not a valid Nostr relay.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('nostr.settings')
      ->set('relay', $form_state->getValue('relay'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
